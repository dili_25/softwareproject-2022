package future_for_friuli_app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@AllArgsConstructor
@ToString
public class Hotel {

//	private static Map<String, Hotel> hotelsByName = new TreeMap<>();
//	private static Map<Integer, Hotel> hotelsByID = new HashMap<>();
	private static ArrayList<Hotel> allHotels = new ArrayList<>();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NonNull
	private String category;
	@NonNull
	private String name;
	@NonNull
	private String owner;
	@NonNull
	private String contact;
	@NonNull
	private String address;
	@NonNull
	private String city;
	@NonNull
	private String cityCode;
	@NonNull
	private String phone;
	@NonNull
	private int nrRooms;
	@NonNull
	private int nrBeds;

	@Override
	public String toString() {
		return address;

	}

	public static ArrayList<Hotel> getAllHotels() {
		return allHotels;
	}

	public static void setAllHotels(ArrayList<Hotel> allHotels) {
		Hotel.allHotels = allHotels;
	}

//	public static Set<String> getHotelNames() {
//		return hotelsByName.keySet();
//	}
//
//	public static Hotel getHotelByName(String name) {
//		return hotelsByName.get(name);
//	}

}
