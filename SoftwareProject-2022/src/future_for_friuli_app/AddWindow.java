package future_for_friuli_app;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class AddWindow extends JFrame implements ActionListener {

	private JPanel contentPane;
	JButton bt_add = new JButton("add"); // buttons,labels & checkboxes (combobox)
	JLabel lbAddnewhotel = new JLabel("Add new Hotel Data");
	JLabel lbCategory = new JLabel("Category");
	JLabel lbName = new JLabel("Name");
	JLabel lbAdress = new JLabel("Adress");
	JLabel lbCity = new JLabel("City");
	JLabel lbCityCode = new JLabel("City code");
	JLabel lbOwner = new JLabel("Owner");
	JLabel lbPhone = new JLabel("Phone");
	JLabel lbContact = new JLabel("Contact");
	JLabel lbnrRooms = new JLabel("Nr. of Rooms");
	JLabel lbnrBeds = new JLabel("Nr. of Beds");
	JLabel lbHotelId = new JLabel("Hotel ID");
	JComboBox<Category> cbCategory = new JComboBox<Category>(Category.values());

	private JTextField txtName;
	private JTextField txtOwner;
	private JTextField txtContact;
	private JTextField txtAdress;
	private JTextField txtCity;
	private JTextField txtCityCode;
	private JTextField txtPhone;
	private JTextField txtNrRooms;
	private JTextField txtNrBeds;
	private JTextField txtHotelId;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddWindow frame = new AddWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddWindow() {
		setTitle("add hotel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		bt_add.setBounds(185, 241, 85, 21);
		contentPane.add(bt_add);

		JLabel lbAddnewhotel = new JLabel("Add new Hotel Data");
		lbAddnewhotel.setBounds(10, 10, 416, 13);
		contentPane.add(lbAddnewhotel);

		JLabel lbCategory = new JLabel("Category:");
		lbCategory.setBounds(10, 29, 61, 13);
		contentPane.add(lbCategory);

		cbCategory.setBounds(91, 29, 50, 17);
		contentPane.add(cbCategory);

		JLabel lbName = new JLabel("Name:");
		lbName.setBounds(10, 55, 61, 13);
		contentPane.add(lbName);

		txtName = new JTextField();
		txtName.setBounds(91, 52, 335, 19);
		contentPane.add(txtName);
		txtName.setColumns(10);

		JLabel lbOwner = new JLabel("Owner:");
		lbOwner.setBounds(10, 84, 61, 13);
		contentPane.add(lbOwner);

		txtOwner = new JTextField();
		txtOwner.setBounds(91, 81, 335, 19);
		contentPane.add(txtOwner);
		txtOwner.setColumns(10);

		JLabel lbContact = new JLabel("Contact:");
		lbContact.setBounds(10, 113, 61, 13);
		contentPane.add(lbContact);

		txtContact = new JTextField();
		txtContact.setBounds(91, 110, 335, 19);
		contentPane.add(txtContact);
		txtContact.setColumns(10);

		JLabel lbAdress = new JLabel("Adress:");
		lbAdress.setBounds(10, 142, 61, 13);
		contentPane.add(lbAdress);

		txtAdress = new JTextField();
		txtAdress.setBounds(91, 139, 335, 19);
		contentPane.add(txtAdress);
		txtAdress.setColumns(10);

		JLabel lbCity = new JLabel("City:");
		lbCity.setBounds(10, 168, 61, 13);
		contentPane.add(lbCity);

		txtCity = new JTextField();
		txtCity.setBounds(91, 168, 126, 16);
		contentPane.add(txtCity);
		txtCity.setColumns(10);

		JLabel lbCityCode = new JLabel("City code:");
		lbCityCode.setBounds(227, 168, 61, 13);
		contentPane.add(lbCityCode);

		txtCityCode = new JTextField();
		txtCityCode.setBounds(294, 166, 132, 16);
		contentPane.add(txtCityCode);
		txtCityCode.setColumns(10);

		JLabel lbPhone = new JLabel("Phone:");
		lbPhone.setBounds(10, 192, 61, 13);
		contentPane.add(lbPhone);

		txtPhone = new JTextField();
		txtPhone.setBounds(91, 192, 335, 19);
		contentPane.add(txtPhone);
		txtPhone.setColumns(10);

		JLabel lbnrRooms = new JLabel("Nr. of Rooms:");
		lbnrRooms.setBounds(10, 218, 78, 13);
		contentPane.add(lbnrRooms);

		txtNrRooms = new JTextField();
		txtNrRooms.setBounds(91, 215, 126, 19);
		contentPane.add(txtNrRooms);
		txtNrRooms.setColumns(10);

		JLabel lbnrBeds = new JLabel("Nr. of Beds:");
		lbnrBeds.setBounds(223, 218, 69, 13);
		contentPane.add(lbnrBeds);

		txtNrBeds = new JTextField();
		txtNrBeds.setBounds(296, 215, 130, 19);
		contentPane.add(txtNrBeds);
		txtNrBeds.setColumns(10);

//		JLabel lbHotelId = new JLabel("Hotel ID:");
//		lbHotelId.setBounds(151, 29, 56, 13);
//		contentPane.add(lbHotelId);
//
//		txtHotelId = new JTextField();
//		txtHotelId.setBounds(206, 26, 220, 19);
//		contentPane.add(txtHotelId);
//		txtHotelId.setColumns(10);

		bt_add.addActionListener(this); // checkbox, buttons (sortieren nach monaten --> actionlist)
		//cbCategory.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Jetzt kommt die add methode:
		if (e.getSource() == bt_add) {
			saveHotel(cbCategory.getSelectedItem().toString(), txtName.getText(), txtOwner.getText(), txtContact.getText(), txtAdress.getText(), txtCity.getText(), txtCityCode.getText(), txtPhone.getText(), Integer.parseInt(txtNrRooms.getText()), Integer.parseInt(txtNrBeds.getText()));
		}
	}
	
	public void saveHotel(String category, String name, String owner, String contact, String adress, String city, String cityCode, String phone, int nrRooms, int nrBeds) {
		Hotel h = new Hotel(category, name, owner, contact, adress, city, cityCode, phone, nrRooms, nrBeds);
		HotelDAO_.persist(h);
	}
}
