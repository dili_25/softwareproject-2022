package future_for_friuli_app;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HotelDAO_ {

	public static void main(String[] args) {

	}

	private static Hotel search(int id) {
		Session session = getOpenSession();

		Hotel h = (Hotel) session.get(Hotel.class, id);
		return h;
	}

	public static int persist(Hotel h) {
		Session session = getOpenSession();

		try {
			session.beginTransaction();
			session.save(h);
			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return h.getId();

	}

	public static void persist2(ArrayList<Hotel> allHotels) {
		Session session = getOpenSession();

		try {

			// session.save(h)

			session.beginTransaction();
			for (Hotel hotel : allHotels) {
				session.save(hotel);
			}

			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		System.out.println("Done: " + allHotels.size());
	}
	
	public static boolean delete(Hotel hotel) {
		Session session = getOpenSession();
		try {
			session.beginTransaction();
			session.delete(hotel);

			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			session.close();
		}
		session.close();
		return true;
	}
	
	public static List<Hotel> getAllHotels() {
		Session session = getOpenSession();
		List<Hotel> hotels = new ArrayList<Hotel>();
		try {
			session.beginTransaction();
			hotels = session.createQuery("FROM Hotel", Hotel.class).list();  

			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		session.close();
		return hotels;
	}

	private static Session getOpenSession() {
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Hotel.class)
				.buildSessionFactory();
		return factory.openSession();
	}

}
