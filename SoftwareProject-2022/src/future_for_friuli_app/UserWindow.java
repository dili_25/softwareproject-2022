package future_for_friuli_app;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;


public class UserWindow extends JFrame implements ActionListener{
	private JTextArea ta_text = new JTextArea(20, 20);
	private JButton bt_print = new JButton("Print the document");
	
	
	
	
		
	public UserWindow() {

		setLayout(new FlowLayout());
		setTitle("User Window");
		setSize(600, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		add(ta_text);
		add(bt_print);

		
		bt_print.addActionListener(this);
		
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == bt_print) {
			System.out.println("Detected");
		//	PrintIntro.printComponenet(ta_text);
			
		}
		
	}

}


