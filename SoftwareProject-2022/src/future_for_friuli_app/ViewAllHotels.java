package future_for_friuli_app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.checkerframework.checker.units.qual.m;

import lombok.Getter;

@Getter
public class ViewAllHotels extends JPanel {

	private JPanel contentPanel;
	private JTable hotelTable;
	DefaultTableModel model = new DefaultTableModel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewAllHotels frame = new ViewAllHotels();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewAllHotels() {
		contentPanel = new JPanel(new BorderLayout());

		JPanel top = new JPanel(new FlowLayout());
		top.add(new JLabel("Hotelübersicht:"));
		contentPanel.add(top, BorderLayout.NORTH);
		

		List<Hotel> allHotels = HotelDAO_.getAllHotels();
		Object[][] tableData = new Object[allHotels.size()][4];
		for (int i = 0; i < allHotels.size(); i++) {
			Hotel h = allHotels.get(i);
			tableData[i][0] = h.getName();
			tableData[i][1] = h.getCategory();
			tableData[i][2] = h.getNrRooms();
			tableData[i][3] = h.getNrBeds();
		}

		String[] tableColumns = new String[] { "Name", "Kategorie", "Zimmer", "Betten" };

		hotelTable = new JTable(tableData, tableColumns);

		JScrollPane scrollpane = new JScrollPane(hotelTable);
		
		contentPanel.add(scrollpane, BorderLayout.CENTER);


		JPanel buttonPanel = new JPanel(new GridLayout());
		JButton btnCreate = new JButton("Hotel anlegen");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnClickCreateHotel(e);
			}
		});
		buttonPanel.add(btnCreate);
		JButton btnEdit = new JButton("Hotel bearbeiten");
		btnEdit.setActionCommand("edit");
		// btnEdit.addActionListener(Main::btnClickEditCreateHotel);
		buttonPanel.add(btnEdit);
		contentPanel.add(buttonPanel, BorderLayout.SOUTH);

	}
	
	private void btnClickCreateHotel(ActionEvent e) {
		DialogHotel dlg = new DialogHotel((JFrame) SwingUtilities.getWindowAncestor(contentPanel));
		Hotel res = dlg.doDialog(true);
		List<Hotel> allHotels = HotelDAO_.getAllHotels();
		Object[][] tableData = new Object[allHotels.size()][4];
		for (int i = 0; i < allHotels.size(); i++) {
			Hotel h = allHotels.get(i);
			tableData[i][0] = h.getName();
			tableData[i][1] = h.getCategory();
			tableData[i][2] = h.getNrRooms();
			tableData[i][3] = h.getNrBeds();
		}

		String[] tableColumns = new String[] { "Name", "Kategorie", "Zimmer", "Betten" };

		hotelTable = new JTable(tableData, tableColumns);
		
		contentPanel.revalidate();
		contentPanel.repaint();
		
	}

	private void loadRows() {
//		HotelDAO_.persist2(Hotel.getAllHotels());

//		model.addRow(new Object[] { hotelid, category, name, owner, contact, adress, city, cityCode, phone, nrRooms, nrBeds });

	}

	private void loadColumns() {
		model.addColumn("ID");
		model.addColumn("Category");
		model.addColumn("Name");
		model.addColumn("Owner");
		model.addColumn("Contact");
		model.addColumn("Address");
		model.addColumn("City");
		model.addColumn("City Code");
		model.addColumn("Phone");
		model.addColumn("Number of rooms");
		model.addColumn("Number of beds");

	}

}
