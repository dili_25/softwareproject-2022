package future_for_friuli_app;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginWindow extends JFrame {

	private JPanel contentPane;
	private JTextField tfUsername;
	private JTextField tfPassword;
	
	private static String REAL_PWD_USER = "user";
	private static String REAL_USER_USER = "user";

	private static String REAL_PWD_ADMIN = "admin";
	private static String REAL_USER_ADMIN = "admin";
	
	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public LoginWindow() {
		setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lbUsername = new JLabel("Username");
		lbUsername.setBounds(10, 20, 416, 13);
		contentPane.add(lbUsername);

		tfUsername = new JTextField();
		tfUsername.setBounds(10, 43, 416, 19);
		contentPane.add(tfUsername);
		tfUsername.setColumns(10);

		JLabel lbPassword = new JLabel("Password");
		lbPassword.setBounds(10, 80, 416, 13);
		contentPane.add(lbPassword);

		tfPassword = new JTextField();
		tfPassword.setBounds(10, 103, 416, 19);
		contentPane.add(tfPassword);
		tfPassword.setColumns(10);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() { //
			
			@Override
			public void actionPerformed(ActionEvent e) {

			String username = tfUsername.getText();
			String pwd = tfPassword.getText();

			if (username.trim().length() == 0 || pwd.trim().length() == 0) {
				JOptionPane.showMessageDialog(null, "No valid data inside");
				return;
			}

			if (username.equals(REAL_USER_USER) && pwd.equals(REAL_PWD_USER)) {
				new UserWindow().setVisible(true);
			} else if (username.equals(REAL_USER_ADMIN) && pwd.equals(REAL_PWD_ADMIN)) {
				new MasterWindow().setVisible(true);
			} else {
				JOptionPane.showMessageDialog(null, "user and /or pwd is incorrect");

			}

			dispose();
}
		})
;
}
}