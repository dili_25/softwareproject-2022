package future_for_friuli_app;

public enum Category {
	ONE("*"), TWO("**"), THREE("***"), FOUR("****"), FIVE("*****");

	private String stars;

	private Category(String stars) {
		this.stars = stars;
	}

	public String toString() {
		return stars;
	}
}
