package future_for_friuli_app;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;


public class RunApplication {

	public static void main(String[] args) {

		// }
		// public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					LoginWindow frame = new LoginWindow();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}

//		new MasterWindow().setVisible(true);
//		new LoginWindow().setVisible(true);
//		new UserWindow().setVisible(true);
//		new AddWindow().setVisible(true);


//												AddHotel x = new AddHotel();
//												x.setVisible(true);
		
		//todo: 
		//window mit textfelder & attribute (von hotel Klasse) davor label (name, category...)
		//Jtextfield.getText, ein hotel erstellen 
		//button (add) --> persist Methode für ein Hotel 
		//im hibernet update 
		
		
		JFrame frame = new JFrame("Friuli");
		frame.setLayout(new BorderLayout());
		JLabel titleLabel = new JLabel("<HTML><SPAN style='color:yellow'>Friuli</SPAN>");
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		titleLabel.setFont(titleLabel.getFont().deriveFont(32.0f));
		titleLabel.setOpaque(true);
		titleLabel.setForeground(Color.WHITE);
		titleLabel.setBackground(Color.BLUE);
		frame.add(titleLabel,BorderLayout.NORTH);
		
		JTabbedPane tp = new JTabbedPane();
		tp.addTab("Hotels", (new ViewAllHotels()).getContentPanel());
		//tp.addTab("Belegung", c(new ViewAllOccupancies()).getContentPanel());
		frame.add(tp,BorderLayout.CENTER);
		
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}
}
