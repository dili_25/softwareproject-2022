package future_for_friuli_app;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class OccupancyDAO_ {
	public static void main(String[] args) {

	}

	private static Occupancy search(int o_id) {
		Session session = getOpenSession();

		Occupancy o = (Occupancy) session.get(Occupancy.class, o_id);
		return o;
	}

	private static int persist(Occupancy o) {
		Session session = getOpenSession();

		try {
			session.beginTransaction();
			session.save(o);
			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return o.getID();

	}

	public static void persist2(List<Occupancy> all) {
		Session session = getOpenSession();

		try {

			// session.save(o)

			session.beginTransaction();
			for (Occupancy o : all) {
				session.save(o);
			}

			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		System.out.println("Done: " + all.size());
	}

	private static Session getOpenSession() {
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Occupancy.class)
				.buildSessionFactory();
		return factory.openSession();
	}

}
