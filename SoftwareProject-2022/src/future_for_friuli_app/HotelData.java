package future_for_friuli_app;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HotelData {

	public static void main(String[] args) throws FileNotFoundException{
		File all = new File("./src/future_for_friuli_app/hotels.txt");
		Scanner scan = new Scanner(all);
		ArrayList<Hotel> allHotels = new ArrayList<>();

		while(scan.hasNext()) {
			String kette = scan.nextLine();
			String[] parts = kette.split(",");

			Hotel h;
			try {
				int id = Integer.parseInt(parts[0]);
				String category = parts[1];
				String hotelname = parts[2];
				String owner = parts[3];
				String contact = parts[4];
				String address = parts[5];
				String city = parts[6];
				String cityCode = parts[7];
				String phone = parts[8];
				int noRooms = Integer.parseInt(parts[9]);
				int noBeds = Integer.parseInt(parts[10]);

				h = new Hotel(id, category, hotelname, owner, contact, address, city, cityCode, phone, noRooms, noBeds);
				allHotels.add(h);
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}

		System.out.println(allHotels.size());
		HotelDAO_.persist2(allHotels);

	}

}
