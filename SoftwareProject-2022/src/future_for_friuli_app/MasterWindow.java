package future_for_friuli_app;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MasterWindow extends JFrame implements WindowListener, ActionListener {
	private JButton bt_x = new JButton(">xy");
	private JButton bt_a = new JButton(">xy");
	private JButton bt_d = new JButton(">xy");
	private JButton bt_f = new JButton(">xy");
	private JButton bt_fr = new JButton(">xy");


	public MasterWindow() {

		setLayout(new FlowLayout());
		setTitle("Master Window");
		setSize(600, 600);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(null);
		add(bt_a, BorderLayout.NORTH);
		addWindowListener(this);

		bt_a.addActionListener(this);

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		System.out.println("Some message");

		int answer = JOptionPane.showInternalConfirmDialog(null, "Are you sure?");
		System.out.println(answer);

		if (answer == 0) {
			System.exit(0);
		}

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == bt_a) {
			System.out.println("a button was clicked");
		} else if (e.getSource() == bt_d) {
			System.out.println("functions a stuff");
		}

	}

}
